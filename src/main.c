#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/coulomb.h"



void showTgt(void);
void showPrj(void);

void setTgt(char* name);
void setPrj(char* name);

void dropAdditionalInfo( FILE* d);



typedef struct {
	char* name;

	int Z_2;
	double I, N;
} target;

typedef struct {
	char* name;
	int Z_1;
	double M_1;
} projectile;

target tgts[] = {
	{ "Fe", 26, 8.0,  84.0 },
	{ "Si", 14, 8.152,49.9 }
};
projectile prjs[] = {
	{ "Fe", 26, 50.0e9 },
	{ "C60", 3, 60*24e9 }
};

const char dropdir[] = ".";

target* ct = &tgts[0];
projectile* cp = &prjs[0];



float step, velo, rmax, time;



int main(){
	char buffer[10], outfname[100];
	
	printf("target> ");
	scanf("%s", buffer );
	setTgt(buffer);

	printf("projectile> ");
	scanf("%s", buffer );
	setPrj(buffer);

	do{
		printf("velo [c]> ");
		scanf("%f", &velo );
	} while( !(velo > 0.0 && velo < 1.0) );
	
	do{
		printf("step [nm]> ");
		scanf("%f", &step );
	} while( step <= 0.0 );

	do{
		printf("r max [nm]> ");
		scanf("%f", &rmax );
	} while( !(rmax > step) );
		
	do{
		printf("time [fs]> ");
		scanf("%f", &time );
	} while( !(time > 0.0 && time < 50) );

	sprintf(outfname,"%s/%s-%s%.3ffs%.3fc.dat",dropdir,cp->name,ct->name,time,velo);
	FILE* outfd = fopen(outfname,"w");
	dropAdditionalInfo(outfd);
	
 	double t, fv, fvv = 0, c = 0;
	fprintf( outfd, "\n# TIME EVOLUTION\n# r=%f\n", rhoMax(velo)/3 );
	for( t = 0; t < time; t+= time/100 ){
		fv = FieldRelaxation(rhoMax(velo), t, velo);
		fvv = Field(rhoMax(velo), t , velo );
        c = Current(rhoMax(velo), t, velo);

		printf("%10.3f, %e %e %e\n", t, fv, fvv, c);
		fprintf(outfd, "%.4f\t%10.7e\t%10.7e\n", t, fv, fvv );
	} 

	double r;
	fprintf(outfd, "\n# SPATIAL\n");	
	for(r = 0; r<=rmax; r+=step){
		fv = Field( r ,time,velo);
		printf("%.4f\t%10.7e\n",r,fv);
		fprintf(outfd,"%.4f\t%10.7e\n",r,fv);
	}
	dropAdditionalInfo(stdout);

	fclose(outfd);
	return 0;
}

void showPrj(){
	printf("# Projectile: %5s\n", cp->name );
}

void showTgt(){
	printf("# Target: %5s\n", ct->name);
}
void setTgt( char* name ){
	int i = 0;

	while( i < sizeof tgts/sizeof(target) && strcmp( name, tgts[i].name ) ) i++;
	if( i == sizeof tgts/sizeof(target) ){
		printf("# No matching target found\n");
		i = 0;	
	}

	ct = &tgts[i];

    setTarget(ct->Z_2, ct->N, ct->I);
	showTgt();
	return;
}

void setPrj( char* name ){
	int i = 0;

	while( i < sizeof prjs/sizeof(projectile) && strcmp( name, prjs[i].name ) ) i++;
	if( i == sizeof prjs/sizeof(projectile) ){
		printf("# No matching projectile found\n");
		i = 0;	
	}

	cp = &prjs[i];

    setProjectile(cp->Z_1, cp->M_1);

	showPrj();
	return;
}

void dropAdditionalInfo(FILE* d ){
	fprintf(d,"# Projectile energy %10.7e [eV]\n", projEnergy(velo));
	fprintf(d,"# Max 6-electron energy %10.7e [eV]\n", elecEnergyMax(velo));
	fprintf(d,"# Max radius %10.7e [nm]\n", rhoMax(velo));
	fprintf(d,"# Character field Magnitude %10.7e [eV nm-1]\n", chFieldMagn(velo));
	fprintf(d,"# Se =  %10.7e [eV nm-1]\n", energyLossesPDU(velo));
}
