#include <stdio.h>
#include <math.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_expint.h>

#include "../include/ph_constants.h"


int Z_1, Z_2;
double  M_1, N, I;

double COU_TR = 1;

#define rhom	rhoMax(v)
#define epsm	elecEnergyMax(v)
#define E_1	projEnergy(v)
#define ZP	effectiveZ(v)

//#define DEBUG
//#define DDEBUG

void setTarget(int z, double n, double i){
    Z_2 = z;
    N = n;
    I = i;
    return;
}

void setProjectile(int z, double m){
    Z_1 = z;
    M_1 = m;
    return;
}

double effectiveZ( double speed ){
	double sps = speed/PH_BHRSPD;
	return Z_1*( 1 - exp( - sps*pow( Z_1, -2.0/3) ) );
}

double projEnergy( double v ){
	return M_1*v*v/2;
}

double elecEnergyMax( double v ){
	return 4*PH_EMASS*E_1/M_1;
}

double rhoMax( double v ){
	return 2*ZP*PH_SQCHG*pow( epsm*I, -0.5 );
}

double chFieldMagn( double v ){
	return 2*PH_SQCHG*N*Z_2*rhom;
}

double energyLossesPDU( double v ){
	return 4*M_PI*N*pow(PH_SQCHG,2.0)*Z_2*pow(ZP,2.0)*log(epsm/I)/epsm;
}

struct eps1params {
	double t, eps0;
};

double qToEps1(double p){
    return 1 + exp(p);
}

void eps1FDF( double q, void* params, double* f, double* df ){
	struct eps1params* p = (struct eps1params*) params;	

	double threshold = -10;
	
	double t = p->t;
	double eps0 = p->eps0;

	double a  = 1.5*log(qToEps1(q) );
    double at = 1.5*log(qToEps1(threshold) );
	double b  = 1.5*log( eps0 );

	double vI = PH_C*sqrt( 2*I/PH_EMASS );
	double K = M_PI*Z_2*pow(PH_SQCHG,2.0)*vI*t*N/pow(I,2.0);

	*f = K - (gsl_sf_expint_Ei(b) - ((q > threshold)? gsl_sf_expint_Ei(a) : (gsl_sf_expint_Ei(at) - (threshold - q))));

	*df = ( q > threshold )? exp( exp( q ) ) : 1.0;

	#ifdef DDEBUG
	printf("eps1FDF> x = %10.7e f = %10.7e df = %10.7e\n", q , *f, *df ); 
	#endif
	return;
} 

double eps1F( double q, void* params ){
	double f, df;
	eps1FDF( q, params, &f, &df );
	return f;
}

double eps1DF( double q, void* params ){
	double f, df;
	eps1FDF( q, params, &f, &df );
	return df;
}

void prebisect( double (*func)( double, void* ), void* p, double* xlo, double* xhi, int* error){
	#ifdef DDEBUG
	printf("pb> right = %.7e\n", *xhi );
	#endif

	int iter = 0, maxiter = 5000;
	
	*xlo = *xhi;

	do{
		iter++;
		*xhi = *xlo;
		*xlo -= 10;
	} while( func(*xlo,p)*func(*xhi,p) >= 0  );
	
	if( iter >= maxiter ) *error = 1;
	else *error = 0;
}

/**
 *
 * @param eps0 [-]
 * @param t
 * @return q
 *
 * eps1 = I*(1 + exp(p))
 */
double qeps1fun(double eps0, double t){
	if(eps0 == 1.0) return -INFINITY;

	int max_iter = 1000;
	double guess = eps0;

	struct eps1params p = { t, eps0 }; 
	
	
	const gsl_root_fsolver_type* T = gsl_root_fsolver_brent;
	gsl_root_fsolver* s	= gsl_root_fsolver_alloc( T );
	 
	gsl_function F;
	F.function = &eps1F;
	F.params = &p;
	

	int iter = 0;
	double x;
	int status;
	double xlo, xhi = log(eps0 - 1);

	prebisect( &eps1F, &p, &xlo, &xhi, &status );

	#ifdef DEBUG
	printf("eps1> Obtaining eps1 for: t = %5e eps0 = %10.7e \n", t, eps0 );
	printf("eps1> prebisect> %s: xlo = %10.7e xhi = %10.7e\n", (!status)? "success" : "error", xlo, xhi ); 
	#endif

	#ifdef DDEBUG
	printf("eps1> %-5s %10s %10s\n", "iter", "root", "est");
	#endif

	gsl_root_fsolver_set(s, &F, xlo, xhi);
	do {
		gsl_root_fsolver_iterate(s);
		x = gsl_root_fsolver_root(s);
		xlo = gsl_root_fsolver_x_lower(s);
		xhi = gsl_root_fsolver_x_upper(s);

		iter++;
		
		status = gsl_root_test_interval(xlo, xhi, 0, 1e-7);
		
		#ifdef DDEBUG
		printf("eps1> %5d %10.7e %+10.7e\n", iter, x, xhi - xlo);
		if( status == GSL_SUCCESS ) printf("eps1> Converged\n");
		#endif
	}
	while( status == GSL_CONTINUE && iter < max_iter );

	if( iter >= max_iter ){
		printf("eps1> Required precision wasn't reached for %d iterations", max_iter);
		abort();
	}	
	
	gsl_root_fsolver_free(s);		
	return x;
}

/**
 *
 * @param q
 * @param eps0 [-]
 * @param psi = epsm/I [-]
 * @return r(q) [nm]
 */
double r1fun(double q, double eps0, double psi){
	if(eps0 == 1.0) return 0.0;


	double threshold = -5;
//	double a = log(4.0/3) + q;
    double a  = 2*log(qToEps1(q) );
    double at = 2*log(qToEps1(threshold));
    double b  = 2*log( eps0 );


	double K = pow(I,2.0)/( M_PI*Z_2*N*pow(PH_SQCHG,2) );
	return K*sqrt( 1 - eps0/psi )*(	gsl_sf_expint_Ei(b) - ((q > threshold)? gsl_sf_expint_Ei(a) : gsl_sf_expint_Ei(at) - (threshold - q )
		));
}

double tauFun(double q, double eps0, double t){
    if(eps0 == 1.0) return 0.0;

    double threshold = -5;

    double a  = 1.5*log(qToEps1(q) );
    double at = 1.5*log(qToEps1(threshold) );
    double b  = 1.5*log( eps0 );

    double vI = PH_C*sqrt( 2*I/PH_EMASS );
    double rr = pow(I,2.0)/(M_PI*Z_2*pow(PH_SQCHG,2.0)*vI*N);

    return t - rr/vI*(gsl_sf_expint_Ei(b) - ((q > threshold)? gsl_sf_expint_Ei(a) : (gsl_sf_expint_Ei(at) - (threshold - q))));
}

struct FIIparams {
	double A, B, C;
};

double fieldInnerIntegrand( double x, void* params ){
	struct FIIparams* p = (struct FIIparams*) params;
	
	double
		A = p->A,
		B = p->B,
		C = p->C;
 
	return 1/x  *  (1 - (A/2 + B/2 - x)/C)  *  
			1.0/sqrt(x - A)  *  1.0/sqrt(B - x); 
}

double FILPartial( double x, void* params ){
	struct FIIparams* p = (struct FIIparams*) params;
	
	double
		A = p->A,
		B = p->B,
		C= p->C;

	if( A == 0.0 ) return 2*atan(sqrt(x/(B-x)))/C;
 
	return  2*atan(sqrt( (x - A)/A ))/sqrt(A) * 
			(1 - (A/2 + B/2 - x)/C)/sqrt(B - x); 
}

double FIIL( double x, void* params ){
	struct FIIparams* p = (struct FIIparams*) params;
	
	double
		A = p->A,
		B = p->B,
		C = p->C,
		D = (A + B)/2;
	
	if( A == 0.0) return 0.0;

	if( x <= A ) return 0;	
 
	return  - 2*atan(sqrt( (x - A)/A ))/sqrt(A) * 
		( 2*B + C - D - x )/( 2*C*pow(sqrt(B-x),3.0) ); 
}

double FIRPartial( double x, void* params ){
	struct FIIparams* p = (struct FIIparams*) params;
	
	double
		A = p->A,
		B = p->B,
		C= p->C;
 
	return  (-2)*atan(sqrt( (B - x)/B ))/sqrt(B) * 
			(1 - (A/2 + B/2 - x)/C)/sqrt(x - A); 
}

double FIIR( double x, void* params ){
	struct FIIparams* p = (struct FIIparams*) params;
	
	double
		A = p->A,
		B = p->B,
		C = p->C,
		D = (A + B)/2;
	
	if( x >= B ) return 0;	
 
	return  - (-2)*atan(sqrt( (B - x)/B ))/sqrt(B) * 
		( -2*A - C + D + x )/( 2*C*pow(sqrt(x-A),3.0) ); 
}

struct FOIparams {
	double r, t, rm, psi;	
};

int FOIcounter = 0;

double fieldOuterIntegrand( double eps0, void* params ){
	FOIcounter++;
	struct FOIparams* p = (struct FOIparams*) params;

	double
		psi = p->psi,
		r   = p->r,
		rm  = p->rm,
		t   = p->t;
		
	double xi = r/rm;

	double r0 = sqrt( 1.0/eps0 - 1.0/psi );

	double B = pow(xi + r0,2.0);
	double A = pow(xi - r0,2.0);
	double C = 2*pow(xi, 2.0 );
	struct FIIparams par = {A, B, C};

	double 
		qeps1 = qeps1fun(eps0, t),
		r1sq = pow(r1fun( qeps1, eps0, psi )/rm, 2.0),
		xd = A, 
		xu = ( r1sq > B )? B : r1sq;

    double tau = tauFun(qeps1,eps0,t);
	double FIIresult, FIIerror;

	double FIILresult, FIILerror;
	double FIIRresult, FIIRerror;

	#ifdef DEBUG
	printf("%6d ", FOIcounter);
	printf("FOI> integration xlo = %10.7e xhi = %10.7e\n", xd, xu);
	#endif

	if( xd < xu ){
		gsl_integration_workspace * w = gsl_integration_workspace_alloc(1000);
		gsl_function F,G;
//		F.function = &fieldInnerIntegrand;

		F.function = &FIIL;
		F.params = &par;

		G.function = &FIIR;
		G.params = &par;

		gsl_integration_qags( &F, xd, xu - (xu-xd)/2, 0, 1e-4, 1000, w, &FIILresult, &FIILerror );
		gsl_integration_qags( &G, xu - (xu-xd)/2, xu, 0, 1e-4, 1000, w, &FIIRresult, &FIIRerror );
		
		gsl_integration_workspace_free(w);	

		FIILresult += FILPartial(xu-(xu-xd)/2, &par ) - FILPartial(xd,&par);
		FIIRresult += FIRPartial(xu, &par ) - FIRPartial(xu-(xu-xd)/2,&par);

		FIIresult = FIILresult + FIIRresult;	
	}
	else {
		FIIresult = FIIerror = 0.0;
	}

	if( isnan( FIIresult ) || isinf(FIIresult )){
		printf("FOI> NAN/INF result\n");
		abort();
	}
	
	#ifdef DEBUG
	printf("%6d ", FOIcounter);
	printf("FOI> result %e\n", FIIresult );
	#endif

	return FIIresult/pow(eps0,2.0);
}

/**
 *
 * @param eps0 [-]
 * @param params = {epsm/I [-], r [nm], rm [nm], t}
 * @return [-]
 */

double currentIntegrand( double eps0, void* params ) {
    struct FOIparams *p = (struct FOIparams *) params;

    double
            psi = p->psi,
            r = p->r,
            rm = p->rm,
            t = p->t;

    double qeps1 = qeps1fun(eps0, t);
    double r1sq  = pow(r1fun(qeps1, eps0, psi)/rm, 2.0);
    double r0 = sqrt( 1.0/eps0 - 1.0/psi ); // [-]
    double xi = r/rm;

    double A = pow(xi - r0, 2.0);
    double B = pow(xi + r0, 2.0);
    double C = 2*xi*xi;

    if (A >= r1sq) return 0.0;
    if (B <= r1sq) return 0.0;

    return
            1.0/pow(eps0, 2.0) *
                    sqrt(1 - eps0/psi) *
                    sqrt(qToEps1(qeps1)) * // velocity
                    xi/sqrt(r1sq) * // rho/r1
                    (1 - (A/2 + B/2 - r1sq)/C)/(sqrt(r1sq - A)*sqrt(B - r1sq));


}


double Field(double rho, double t, double v ){
	#ifdef DEBUG
	printf("FFF> r = %5.3f, t = %5.3f, v = %5.3f\n", rho, t, v ); 
	printf("FFF> Z_1 = %d, Z_2 = %d\n", Z_1, Z_2);
	#endif

	if(t == 0.0) return 0.0;

	struct FOIparams par = { rho, t, rhom, epsm/I };

	gsl_integration_cquad_workspace *w = gsl_integration_cquad_workspace_alloc( 1000 );
	gsl_function F;
	F.function = &fieldOuterIntegrand;
	F.params = &par;


	double FOIresult = 0, FOIerror = 0;

	#ifdef DEBUG
	printf("FFF> integration f: %10.7e t: %10.7e\n", 1.0, epsm/I);
	#endif
	
	size_t nevals;
	gsl_integration_cquad( &F, 1.000, epsm/I, 0, 1e-2, w, &FOIresult, &FOIerror, &nevals );
	gsl_integration_cquad_workspace_free(w);	

	#ifdef DEBUG
	printf("FFF> integration error %e\n", FOIerror ); 
	#endif

	return 2*PH_SQCHG*N*Z_2*rho*FOIresult; 
  
}

double Current(double rho, double t, double v ){
#ifdef DEBUG
    printf("CCC> r = %5.3f, t = %5.3f, v = %5.3f\n", rho, t, v );
	printf("CCC> Z_1 = %d, Z_2 = %d\n", Z_1, Z_2);
#endif

    if(t == 0.0) return 0.0;

    struct FOIparams par = { rho, t, rhom, epsm/I };

    gsl_integration_cquad_workspace *w = gsl_integration_cquad_workspace_alloc( 1000 );
    gsl_function F;
    F.function = &currentIntegrand;
    F.params = &par;


    double FOIresult = 0, FOIerror = 0;

#ifdef DEBUG
    printf("CCC> integration f: %10.7e t: %10.7e\n", 1.0, epsm/I);
#endif

    size_t nevals;
    gsl_integration_cquad( &F, 1.000, epsm/I, 0, 1e-2, w, &FOIresult, &FOIerror, &nevals );
    gsl_integration_cquad_workspace_free(w);

#ifdef DEBUG
    printf("CCC> integration error %e\n", FOIerror );
#endif

    return FOIresult;

}

struct FRIparams {
	double rho, t, v;
};

double __FieldRelaxationIntegrand( double tp, void* params ){
	struct FRIparams* p = (struct FRIparams*) params;
	double 
		v = p->v,
		t = p->t,
		rho = p->rho;

	return Field( rho, tp, v )*exp( (tp - t)/COU_TR )/COU_TR; 
}

double FieldRelaxation( double rho, double t, double v ){
	if( t == 0 ) return 0.0;
//	else if( t < COU_TR/5 ) return Field( rho, t, v );

	struct FRIparams intpar = {.rho=rho,.t=t,.v=v};
	double intres, interr;

	gsl_integration_workspace *w = gsl_integration_workspace_alloc( 500 );
        gsl_function F;

        F.function = &__FieldRelaxationIntegrand;
        F.params = &intpar;
	
	gsl_integration_qags( &F, 0.0, t, 0, 1e-2, 500, w, &intres, &interr );
	gsl_integration_workspace_free(w);

	return Field( rho, t, v ) - intres;
}



