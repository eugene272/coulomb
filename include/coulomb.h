#ifndef coulomb_h__
#define coulomb_h__

extern double
Field( double r, double t, double v ),
Current( double r, double t, double v ),
FieldRelaxation( double r, double t, double v), 
energyLossesPDU( double v ),
chFieldMagn( double v ),
projEnergy(double v),
elecEnergyMax( double v ),
rhoMax( double v );

extern void
setTarget(int z, double n, double i),
setProjectile(int z, double m);
#endif
