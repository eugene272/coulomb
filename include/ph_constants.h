#ifndef PH_CONSTS_
#define PH_CONSTS_

#define PH_PLNK 197.327 // ev nm
#define PH_BLTZ 8.617e-5 // ev K-1
#define PH_FSC  (1.0/137)
#define PH_SQCHG  PH_FSC*PH_PLNK
#define PH_BHRSPD PH_FSC // c
#define PH_C 299.9 // nm fs-1
#define PH_BHRRAD 0.0529 // nm
#define PH_EMASS  0.511e6

#endif
